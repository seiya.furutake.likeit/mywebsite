CREATE DATABASE assignment DEFAULT CHARACTER SET utf8;

USE assignment;

CREATE TABLE user(
    user_id int AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    email varchar(255) UNIQUE NOT NULL,
    password varchar(255) NOT NULL,
    PRIMARY KEY(user_id)
)
;

CREATE TABLE todo(
    id int AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    user_id int(255),
    PRIMARY KEY(id)
)
;

ALTER TABLE todo ADD FOREIGN KEY(user_id) REFERENCES user(user_id);

INSERT user (name, email, password)VALUES("田中太郎","legria3224@yahoo.co.jp", "password");

INSERT todo (title, user_id)VALUES("アイロン",1);

DELETE FROM user WHERE user_id=1;
