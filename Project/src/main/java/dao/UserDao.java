package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;
import controller.TodoHelper;
import dto.User;

public class UserDao {

    /**
   * ユーザーIDを取得
   *
   * @param email email
   * @param password パスワード
   * @return String emailとパスワードが正しい場合対象のユーザー名 正しくない||登録されていない場合はnull
   * @throws SQLException 呼び出し元にスロー
   */
  public static User getUser(String email, String password) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM user WHERE email = ?");
      st.setString(1, email);

      ResultSet rs = st.executeQuery();

      User user = null;
      while (rs.next()) {
        if (TodoHelper.getSha256(password).equals(rs.getString("password"))) {
          user = new User();
          user.setId(rs.getInt("user_id"));
          user.setName(rs.getString("name"));
          user.setEmail(rs.getString("email"));
          user.setPassword(rs.getString("password"));
          System.out.println("login succeeded");
          break;
        }
      }

      System.out.println("searching name by email has been completed");
      return user;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }
}
