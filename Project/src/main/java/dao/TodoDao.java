package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import dto.Todo;

public class TodoDao {

  /**
   * Todo一覧取得
   * 
   * @param userId
   * @return
   * @throws SQLException
   */
  public static ArrayList<Todo> getTodosByUserId(int userId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("SELECT * FROM todo WHERE user_id = ? ORDER BY id DESC");
      st.setInt(1, userId);

      ResultSet rs = st.executeQuery();
      ArrayList<Todo> todoList = new ArrayList<Todo>();

      while (rs.next()) {
        Todo todo = new Todo();
        todo.setId(rs.getInt("id"));
        todo.setTitle(rs.getString("title"));
        todo.setUserId(rs.getInt("user_id"));
        todoList.add(todo);
      }
      System.out.println("get Todos by userId has been completed");
      return todoList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  /**
   * Todo検索
   * 
   * @param userId
   * @param searchWord
   * @return
   * @throws SQLException
   */
  public static ArrayList<Todo> getTodosByTitle(int userId, String searchWord) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement(
          "SELECT * FROM todo WHERE user_id = ? AND title LIKE ? ORDER BY id DESC");
      st.setInt(1, userId);
      st.setString(2, "%" + searchWord + "%");

      ResultSet rs = st.executeQuery();
      ArrayList<Todo> todoList = new ArrayList<Todo>();

      while (rs.next()) {
        Todo todo = new Todo();
        todo.setId(rs.getInt("id"));
        todo.setTitle(rs.getString("title"));
        todo.setUserId(rs.getInt("user_id"));
        todoList.add(todo);
      }
      System.out.println("get Todos by searchWord has been completed");
      return todoList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  /**
   * データの挿入処理を行う。
   *
   * @param todo 対応したデータを保持しているTodo
   * @throws SQLException 呼び出し元にcatchさせるためにスロー
   */
  public static void insertTodo(Todo todo) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("INSERT todo(title,user_id) VALUES(?,?)");
      st.setString(1, todo.getTitle());
      st.setInt(2, todo.getUserId());
      st.executeUpdate();
      System.out.println("inserting todo has been completed");
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

    /**
   * IDからTodo情報を取得する
   *
   * @param id ID
   * @return Todo 引数から受け取った値に対応するデータを格納する
   * @throws SQLException 呼び出し元にcatchさせるためスロー
   */
  public static Todo getTodoById(int id) throws SQLException {
    Todo todo = null;
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("SELECT id,title,user_id FROM todo WHERE id =" + id);
      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        todo = new Todo();
        todo.setId(rs.getInt("id"));
        todo.setTitle(rs.getString("title"));
        todo.setUserId(rs.getInt("user_id"));
      }

      st.close();

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }

    System.out.println("searching Todo by id has been completed");
    return todo;
    }

  /**
   * ユーザー情報の更新処理を行う。
   *
   * @param user 対応したデータを保持しているJavaBeans
   * @throws SQLException 呼び出し元にcatchさせるためにスロー
   */
  public static void updateTodo(Todo todo) throws SQLException {
    Todo updatedTodo = new Todo();
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("UPDATE todo SET title=?, user_id=? WHERE id=?;");
      st.setString(1, todo.getTitle());
      st.setInt(2, todo.getUserId());
      st.setInt(3, todo.getId());
      st.executeUpdate();
      System.out.println("update has been completed");

      st = con.prepareStatement("SELECT id,title,user_id FROM todo WHERE id=" + todo.getId());
      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        updatedTodo.setId(rs.getInt("id"));
        updatedTodo.setTitle(rs.getString("title"));
        updatedTodo.setUserId(rs.getInt("user_id"));
      }

      st.close();
      System.out.println("searching updated-Todo has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  /**
   * Todoの削除。
   *
   * @param todo 対応したデータを保持しているTodo
   * @throws SQLException 呼び出し元にcatchさせるためにスロー
   */
  public static void deleteTodo(Todo todo) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("DELETE FROM todo WHERE id = ?");
      st.setInt(1, todo.getId());
      st.executeUpdate();
      System.out.println("deleting todo has been completed");
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }
}
