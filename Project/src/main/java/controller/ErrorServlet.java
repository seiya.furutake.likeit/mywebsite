
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ErrorServlet
 */
@WebServlet("/ErrorServlet")
public class ErrorServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
        if (!isLogin) {
            // Login画面にリダイレクト
            response.sendRedirect("LoginServlet");
        } else {
            String errorMessage = session.getAttribute("errorMessage") != null ? (String) TodoHelper.cutSessionAttribute(session, "errorMessage") : "不正なアクセス";
            request.setAttribute("errMsg", errorMessage);
            request.getRequestDispatcher(TodoHelper.ERROR_PAGE).forward(request, response);
        }
    }

}
