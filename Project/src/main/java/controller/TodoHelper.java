package controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpSession;

/**
 * 定数保持など
 */
public class TodoHelper {
  static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
  static final String LIST_PAGE = "/WEB-INF/jsp/list.jsp";
  static final String CREATE_PAGE = "/WEB-INF/jsp/create.jsp";
  static final String UPDATE_PAGE = "/WEB-INF/jsp/update.jsp";
  static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";

  public static TodoHelper getInstance() {
    return new TodoHelper();
  }

    /**
   * セッションから指定データを取得（削除も一緒に行う）
   *
   * @param session
   * @param str
   * @return
   */
  public static Object cutSessionAttribute(HttpSession session, String str) {
    Object test = session.getAttribute(str);
    session.removeAttribute(str);
    return test;
    }

  /**
   * ハッシュ関数
   *
   * @param target
   * @return
   */
  public static String getSha256(String target) {
    MessageDigest md = null;
    StringBuffer buf = new StringBuffer();
    try {
      md = MessageDigest.getInstance("SHA-256");
      md.update(target.getBytes());
      byte[] digest = md.digest();

      for (int i = 0; i < digest.length; i++) {
        buf.append(String.format("%02x", digest[i]));
      }
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return buf.toString();
  }

}