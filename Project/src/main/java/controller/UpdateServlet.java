package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.Todo;
import dto.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      HttpSession session = request.getSession();
      Boolean isLogin =
          session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
              : false;

      if (!isLogin) {
        // Login画面にリダイレクト
        response.sendRedirect("LoginServlet");
      } else {
        try {
          int id = Integer.parseInt(request.getParameter("id"));
          Todo todo = TodoDao.getTodoById(id);
          request.setAttribute("todo", todo);
          request.setAttribute("title", todo.getTitle());

          request.getRequestDispatcher(TodoHelper.UPDATE_PAGE).forward(request, response);
        } catch (Exception e) {
          e.printStackTrace();
          session.setAttribute("errorMessage", e.toString());
          response.sendRedirect("Error");
        }
      }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      try {
        Boolean isLogin =
            session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
                : false;
        int inputId =
            request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
        String inputTitle = request.getParameter("title");
        int inputUserId = request.getParameter("user_id") != null
            ? Integer.parseInt(request.getParameter("user_id"))
            : 0;

        if (!isLogin) {
          // Login画面にリダイレクト
          response.sendRedirect("LoginServlet");
        } else if (inputTitle.isEmpty()) {
          // titleが未入力
          request.setAttribute("errMsg", "titleが入力されていません");
          Todo todo = TodoDao.getTodoById(inputId);
          request.setAttribute("todo", todo);
          request.setAttribute("title", todo.getTitle());
          request.getRequestDispatcher(TodoHelper.UPDATE_PAGE).forward(request, response);
        } else {
          User user = (User) session.getAttribute("user");
          Todo todo = new Todo();
          todo.setId(inputId);
          todo.setTitle(inputTitle);
          todo.setUserId(inputUserId);

          TodoDao.updateTodo(todo);
          response.sendRedirect("ListServlet");
        }
      } catch (Exception e) {
        e.printStackTrace();
        session.setAttribute("errorMessage", e.toString());
        response.sendRedirect("ErrorServlet");
      }
    }

}