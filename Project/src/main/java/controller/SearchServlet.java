package controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.Todo;
import dto.User;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // TODO Auto-generated method stub
      response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      try {
        Boolean isLogin =
            session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
                : false;
        if (!isLogin) {
          // Login画面にリダイレクト
          response.sendRedirect("LoginServlet");
        } else {
          User user = (User) session.getAttribute("user");
          String searchWord = request.getParameter("title");

          ArrayList<Todo> todoList = TodoDao.getTodosByTitle(user.getId(), searchWord);
          request.setAttribute("todoList", todoList);
          request.getRequestDispatcher(TodoHelper.LIST_PAGE).forward(request, response);
        }
      } catch (Exception e) {
        e.printStackTrace();
        session.setAttribute("errorMessage", e.toString());
        response.sendRedirect("ErrorServlet");
      }
    }

}