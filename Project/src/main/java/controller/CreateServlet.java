package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.Todo;
import dto.User;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      HttpSession session = request.getSession();
      Boolean isLogin =
          session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
              : false;

      if (!isLogin) {
        // Login画面にリダイレクト
        response.sendRedirect("LoginServlet");
      } else {
        request.getRequestDispatcher(TodoHelper.CREATE_PAGE).forward(request, response);
      }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      try {
        Boolean isLogin =
            session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
                : false;
        String inputTitle = request.getParameter("title");
        if (!isLogin) {
          // Login画面にリダイレクト
          response.sendRedirect("LoginServlet");
        } else if (inputTitle.isEmpty()) {
          // titleが未入力
          request.setAttribute("errMsg", "titleが入力されていません");
          request.getRequestDispatcher(TodoHelper.CREATE_PAGE).forward(request, response);
        } else {
          User user = (User) session.getAttribute("user");
          Todo todo = new Todo();
          todo.setTitle(inputTitle);
          todo.setUserId(user.getId());
          TodoDao.insertTodo(todo);
          response.sendRedirect("ListServlet");
        }
      } catch (Exception e) {
        e.printStackTrace();
        session.setAttribute("errorMessage", e.toString());
        response.sendRedirect("ErrorServlet");
      }
    }

}