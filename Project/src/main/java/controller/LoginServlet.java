package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import dto.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      HttpSession session = request.getSession();

      // ログイン失敗時に使用するため
      String email = session.getAttribute("email") != null
          ? (String) TodoHelper.cutSessionAttribute(session, "email")
          : "";
      String errMsg = (String) TodoHelper.cutSessionAttribute(session, "errMsg");

      request.setAttribute("email", email);
      request.setAttribute("errMsg", errMsg);

      request.getRequestDispatcher(TodoHelper.LOGIN_PAGE).forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      try {
        // パラメーターから取得
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        // ユーザ名を取得
        User user = UserDao.getUser(email, password);

        // ユーザーIDが取得できたなら
        if (user != null) {
          session.setAttribute("isLogin", true);
          session.setAttribute("user", user);
          response.sendRedirect("ListServlet");
        } else {
          session.setAttribute("email", email);
          session.setAttribute("errMsg", "入力内容が正しくありません");
          response.sendRedirect("LoginServlet");
        }
      } catch (Exception e) {
        e.printStackTrace();
        session.setAttribute("errorMessage", e.toString());
        response.sendRedirect("ErrorServlet");
      }
    }

}