package dto;

import java.io.Serializable;

/**
 * ユーザー
 */
public class Todo implements Serializable {
  private int id;
  private String title;
  private int userId;

    public Todo() {
    this.title = "";
  }

  public int getId() {
    return id;
    }

  public void setId(int id) {
    this.id = id;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
